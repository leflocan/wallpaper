import paramiko

username = "TVn7"
key_filename = "~/.ssh/FICHIER.pub"
ordinateurs = ["Marluck","jarvis","drogo","morpheus","fumsek","chewbacca"]

def get_remote_wallpapers(hosts, username, key_filename):
    wallpapers = []
    for host in hosts:
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(host, username=username, key_filename=key_filename)

            # Get the wallpaper path on the remote machine
            command = "gsettings get org.gnome.desktop.background picture-uri"
            stdin, stdout, stderr = ssh.exec_command(command)
            wallpaper_uri = stdout.read().decode().strip()
            wallpaper_path = wallpaper_uri.replace("file://", "")

            ssh.close()

            wallpapers.append((host, wallpaper_path))
        except paramiko.AuthenticationException:
            print(f"Authentication failed for {host}, please verify your credentials")
        except paramiko.SSHException as ssh_exception:
            print(f"Unable to establish SSH connection to {host}: {ssh_exception}")
        except Exception as exception:
            print(f"An error occurred while getting the wallpaper from {host}: {exception}")

    return wallpapers


wallpapers = get_remote_wallpapers(ordinateurs, username, key_filename)
for host, wallpaper_path in wallpapers:
    print(f"The wallpaper on {host} is located at {wallpaper_path}")