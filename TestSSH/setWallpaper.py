import paramiko

username = "TVn7"
key_filename = "~/.ssh/FICHIER.pub"
ordinateurs = ["Marluck","jarvis","drogo","morpheus","fumsek","chewbacca"]
wallpaper_path = "Toto"

def set_wallpaper(host, username, key_filename, wallpaper_path):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, username=username, key_filename=key_filename)
        command = f"gsettings set org.gnome.desktop.background picture-uri file://{wallpaper_path}"
        ssh.exec_command(command)
        
        # Set the wallpaper style on the remote machine
        wallpaper_style = "10" # 10 corresponds to "Fill"
        ssh.exec_command(f'gsettings set org.gnome.desktop.background picture-options {wallpaper_style}')

        # Refresh the desktop on the remote machine
        ssh.exec_command("dbus-send --session --type=method_call --dest=org.gnome.Shell /org/gnome/Shell org.gnome.Shell.Eval 'global.display.get_default().sync();'")
        
        ssh.close()
    except paramiko.AuthenticationException:
        print("Authentication failed, please verify your credentials")
    except paramiko.SSHException as ssh_exception:
        print(f"Unable to establish SSH connection to {host}: {ssh_exception}")
    except Exception as exception:
        print(f"An error occurred while setting the wallpaper on {host}: {exception}")