'''
Serveur de Fond d'écran pour TVn7.
V0.0.1
@Autor Antonin Le Floch

'''


#sudo mount /dev/sdb1 /mnt/media

import os
from flask import Flask, render_template, request, jsonify, url_for, redirect
import socket
import sys
from pathlib import Path
import numpy as np
import glob
import paramiko


# --------------- USEFULL FUNCTIONS ------------------

def getImages():
    image_list = []
    image_list_temp = glob.glob('wallpaperWebApp/static/imgs/*.jpg')
    for images in image_list_temp:
        image_list.append(images.rsplit('\\', 1)[-1])
    return image_list
    
# --------------- PART ABOUT SET AND GET WALLPAPER ------------------

ordis = ["Marluck","jarvis","drogo","morpheus","fumsek","chewbacca"]
ordinateurs = ["Marluck","jarvis","drogo","morpheus","fumsek","chewbacca"]
ordis = ["localhost"]
wallpapers = []
app = Flask(__name__)

# Config options - Make sure you created a 'config.py' file.
app.config.from_object('config')
# To get one variable, tape app.config['MY_VARIABLE']


username = "TVn7"
key_filename = "~/.ssh/FICHIER.pub"


def get_remote_wallpapers(hosts, username, key_filename):
    wallpapers = []
    for host in hosts:
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(host, username=username, key_filename=key_filename)

            # Get the wallpaper path on the remote machine
            command = "gsettings get org.gnome.desktop.background picture-uri"
            stdin, stdout, stderr = ssh.exec_command(command)
            wallpaper_uri = stdout.read().decode().strip()
            wallpaper_path = wallpaper_uri.replace("file://", "")

            ssh.close()

            wallpapers.append((host, wallpaper_path))
        except paramiko.AuthenticationException:
            print(f"Authentication failed for {host}, please verify your credentials")
        except paramiko.SSHException as ssh_exception:
            print(f"Unable to establish SSH connection to {host}: {ssh_exception}")
        except Exception as exception:
            print(f"An error occurred while getting the wallpaper from {host}: {exception}")

    return wallpapers


wallpapers = get_remote_wallpapers(ordinateurs, username, key_filename)
for host, wallpaper_path in wallpapers:
    print(f"The wallpaper on {host} is located at {wallpaper_path}")



def set_wallpaper(host, username, key_filename, wallpaper_path):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, username=username, key_filename=key_filename)
        command = f"gsettings set org.gnome.desktop.background picture-uri file://{wallpaper_path}"
        ssh.exec_command(command)
        
        # Set the wallpaper style on the remote machine
        wallpaper_style = "10" # 10 corresponds to "Fill"
        ssh.exec_command(f'gsettings set org.gnome.desktop.background picture-options {wallpaper_style}')

        # Refresh the desktop on the remote machine
        ssh.exec_command("dbus-send --session --type=method_call --dest=org.gnome.Shell /org/gnome/Shell org.gnome.Shell.Eval 'global.display.get_default().sync();'")
        
        ssh.close()
    except paramiko.AuthenticationException:
        print("Authentication failed, please verify your credentials")
    except paramiko.SSHException as ssh_exception:
        print(f"Unable to establish SSH connection to {host}: {ssh_exception}")
    except Exception as exception:
        print(f"An error occurred while setting the wallpaper on {host}: {exception}")



# --------------- PART ABOUT WEB FUNCTIONS ------------------


@app.route('/', methods = ['GET', 'POST'])
def index():
    wallpapers = get_remote_wallpapers(ordinateurs, username, key_filename)
    print(wallpapers) 
    if request.method == 'POST':
        nom_ordi = request.form.get('nom_ordi')
        print(nom_ordi)
        return redirect(url_for('param', nom_ordi=nom_ordi))
    return render_template('index.html', wallpapers=wallpapers, ordis=ordinateurs)

@app.route('/param', methods = ['GET', 'POST'])
def param():
    nom_ordi = request.args.get('nom_ordi', None)
    image_list = getImages()
    print("Image list",image_list)
    if request.method == 'POST':
        nom_wallpaper = request.form.get('nom_wallpaper')
        nom_ordi = request.form.get('nom_ordi')
        print("Wallpaper choosed: ", nom_wallpaper)
        print("host:",nom_ordi)
        set_wallpaper(host, username, key_filename, wallpaper_path)
        #setWallpaper("localhost",nom_wallpaper)
        return redirect(url_for('index'))
    return render_template('param.html', image_list=image_list, wallpapers=wallpapers, ordis=ordinateurs, nom_ordi=nom_ordi)


if __name__ == "__main__":
    app.run()