import socket
import os
from os import path, getenv, getcwd
from ctypes import windll
from shutil import copyfile
from PIL import Image
from tempfile import NamedTemporaryFile
import ctypes


# SERVER TO ACCESS LOCAL VALUES OF WALLPPAPER.
# TO RUN ON EVERY MACHINE


class Wallpaper:
    # Get
    @staticmethod
    def get(returnImgObj=False):
        currentWallpaper = getenv(
            'APPDATA') + "\\Microsoft\\Windows\\Themes\\TranscodedWallpaper"
        if returnImgObj == True:
            return Image.open(currentWallpaper)
        else:
            tempFile = NamedTemporaryFile(mode="wb", suffix='.jpg').name
            copyfile(currentWallpaper, tempFile)
            return tempFile

    # Set
    @staticmethod
    def set(wallpaperToBeSet):
        # Check it is a file
        if path.isfile(wallpaperToBeSet):
            wallpaperToBeSet = path.abspath(wallpaperToBeSet)
            # If a JPG, set
            if wallpaperToBeSet.lower().endswith('.jpg') or wallpaperToBeSet.lower().endswith('.jpeg'):
                windll.user32.SystemParametersInfoW(
                    20, 0, path.abspath(wallpaperToBeSet), 3)
                return True
            # If not a JPG, convert and set
            else:
                image = Image.open(wallpaperToBeSet)
                with NamedTemporaryFile(mode="wb", suffix='.jpg') as tempFile:
                    image.save(tempFile, quality=100)
                    windll.user32.SystemParametersInfoW(
                        20, 0, path.abspath(tempFile), 3)
                return True

        # Check it is a Pillow object
        elif str(wallpaperToBeSet).find('PIL'):
            with NamedTemporaryFile(mode="wb", suffix='.jpg') as tempFile:
                image.save(tempFile, quality=100)
                windll.user32.SystemParametersInfoW(
                    20, 0, path.abspath(tempFile), 3)
            return True
        else:
            return False

    # Copy
    @staticmethod
    def copy(copyTo=getcwd(), fileName='wallpaper.jpg'):
        return copyfile(Wallpaper.get(), path.join(path.abspath(copyTo), fileName))


FondEcran = Wallpaper()

import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind the socket to the port

server_address = ('localhost', 10000)
print ('starting up on %s port %s' % server_address)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print ('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print ('connection from', client_address)

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(16)
            print ('received "%s"' % data)
            if data:
                print ( 'sending data back to the client')
                connection.sendall(data)
            else:
                print ('no more data from', client_address)
                break
            
    finally:
        # Clean up the connection
        connection.close()