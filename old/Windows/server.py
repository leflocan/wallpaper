#Partie serveur à mettre sur tous les ordinateurs de TVn7.
#à mettre dans
#C:\Users\TVn7\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

import ctypes,win32con
import socket
from pathlib import Path

from os import getenv, getcwd

host = 'localhost'
port = 8000

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))
    s.listen(1)
    while True:
        conn, address = s.accept()
        with conn:
            buff = conn.recv(512)
            message = buff.decode('utf-8')
            
            # GET return the actual wallpaper
            if message == "get":
                ubuf = ctypes.create_unicode_buffer(512)
                ctypes.windll.user32.SystemParametersInfoW(win32con.SPI_GETDESKWALLPAPER,len(ubuf),ubuf,0)
                print(ubuf.value.rsplit('\\', 1)[-1])
                conn.sendall(f"{ubuf.value}".rsplit('\\', 1)[-1].encode('utf-8'))
                
            # Else set the wallpaper according to the path
            else:
                SPI_SETDESKWALLPAPER = 20 
                filename = Path(message) #Path("P:/wallpapers" + message) à changer!! 
                print(filename)
                if filename.exists():
                    print(f"{filename}")
                    ctypes.windll.user32.SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, f"{filename}".encode('utf-8') , 0)
                    print("set")
                    conn.sendall(f"set : {filename}".encode('utf-8'))